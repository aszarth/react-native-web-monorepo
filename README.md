# Setup:

## Clean all current dependecies installed (node_modules, dists)

- `yarn clean`

## Install all current dependecies

- `yarn`

## Add new dependecies

- `yarn workspace common add XXX`

# How to run

_Requirements: [React Native](https://facebook.github.io/react-native/docs/getting-started.html#native) (last tested on react-native@0.61)_

## Web

- `yarn workspace web start`

## Mobile

- `cd packages/mobile/ios`
- `pod install`
- `cd -`

- `yarn workspace mobile start`
- Run the project
  - [iOS] Via Xcode
    - `yarn xcode` (open the project on Xcode)
    - Press the Run button
  - [Android] Via Android Studio
    - `yarn studio` (open the project on Android Studio)
    - Press the Run button
  - Via CLI
    - _You may need to launch your device emulator before the next command_
    - `$ yarn android` or `$ yarn ios`

# Info

## Specific plataform files

It will share the same code from mobile, unless you create platform-specific files using the .web.tsx extension (also supports .android, .ios, .native, etc).

## Base Ref

Fork from:
https://github.com/brunolemos/react-native-web-monorepo

(#7ea261dbb0f74ed17b0f5f3d295c51b6c7b39f09)

## Guide

https://dev.to/brunolemos/tutorial-100-code-sharing-between-ios-android--web-using-react-native-web-andmonorepo-4pej
